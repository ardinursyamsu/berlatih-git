<?php
    class Frog extends Animal{
        function __construct($nama){
            $this->name = $nama;
        }

        function jump(){
            return "Hop Hop";
        }
    }
?>