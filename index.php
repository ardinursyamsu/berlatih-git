<!DOCTYPE html>
<html>
    <title>OOP Dengan PHP</title>
    <body>
        <?php
            require_once("classes.php");
            $sheep = new Animal("shaun");
            echo "Name : " . $sheep->name . "<br>"; // "shaun"
            echo "Legs : " . $sheep->legs . "<br>"; // 4
            echo "Cold Blooded : ". $sheep->cold_blooded . "<br>"; // "no"
            
            echo "<br>";

            $frog = new Frog("buduk");
            echo "Name : " . $frog->name . "<br>"; // "shaun"
            echo "Legs : " . $frog->legs . "<br>"; // 4
            echo "Cold Blooded : ". $frog->cold_blooded . "<br>"; // "no"
            echo "Jump : " . $frog->jump() . "<br>";

            echo "<br>";

            $ape = new Ape("kera sakti");
            echo "Name : " . $ape->name . "<br>"; // "shaun"
            echo "Legs : " . $ape->legs . "<br>"; // 4
            echo "Cold Blooded : ". $ape->cold_blooded . "<br>"; // "no"
            echo "Jump : " . $ape->yell() . "<br>";



            // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
        ?>
    </body>
<html>