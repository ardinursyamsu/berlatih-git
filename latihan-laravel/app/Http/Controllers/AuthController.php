<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function welcome(Request $request){
        $data = $request->all();

        return view('welcome')->with($data);
    }

    public function register(){
        return view('register');
    }
}
