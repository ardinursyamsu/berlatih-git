<!DOCTYPE html>
<html>
    <head></head>
    <body>
        <h1>Buat account baru</h1>
        <h2>Sign up form:</h2>
        <form method="post" action="/welcome">
            @csrf
            First name: <input type='text' name='fname'><br><br>
            Last name:  <input type='text' name='lname'><br><br>
            Gender: <br><input type='radio' name='gender' value='Laki-laki'> Laki-laki</input><br>
            <input type='radio' name='gender' value='Perempuan'> Perempuan</input><br>
            <input type='radio' name='gender' value='Non biner'> Non biner</input><br><br>
            Nationality: 
            <select name="nationality">
                <option value="USA"> USA</option>
                <option value="Indonesia"> Indonesia</option>
                <option value="Dutch"> Dutch</option>
                <option value="German"> German</option>
            </select><br><br>
            Language spoken: <br>
            <input type="checkbox" name="language1" value="English">
            <label for="language1"> English</label><br>
            <input type="checkbox" name="language2" value="Bahasa Indonesia">
            <label for="language2"> Bahasa Indonesia</label><br>
            <input type="checkbox" name="language3" value="Dutch">
            <label for="language3"> Dutch</label><br>
            <input type="checkbox" name="language4" value="Deutsche">
            <label for="language4"> Deutsche</label><br><br>
            bio: <br><textarea name="bio" rows="10" cols="40"></textarea><br><br>
            <input type='submit' value='Kirim'>
        </form>
    </body>
</html>